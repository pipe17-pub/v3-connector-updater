const lodash = require('lodash')
const v3 = require('./v3')

const changed = (connector, updates) => {
    // Connector Name can never change. displayName can change
    delete updates.connectorName
    delete connector.connectorName
    let changed
    for (const key of Object.keys(updates)) {
        // Special handle for v3 does not give back empty apikey
        if (key === 'webhook') {
            if (updates.webhook.apikey === '') {
                delete updates.webhook.apikey
            }
            if (connector.webhook && connector.webhook.apikey === '') {
                delete connector.webhook.apikey
            }
        }
        if (lodash.isEqual(connector[key], updates[key])) {
            delete updates[key]
        } else {
            console.info(`There are changes. Changed key: ${key}`)
            console.info(`Currently on V3: ${JSON.stringify(connector[key])}`)
            console.info(`Desired: ${JSON.stringify(updates[key])}`)
            changed = true
        }
    }
    return changed
}

const update = async (env, connectorId, connectorKey, updates) => {
    v3.setEnv(env)
    v3.setKey(connectorKey)
    console.info(`Getting connector ${connectorId}`)
    let connector
    try {
        connector = await v3.getConnector(connectorId)
    }
    catch (e) {
        console.error(`Exception when get connector: `, e)
        throw e
    }
    console.info(`Got the connector successfully. Check if the updates change anything.`)
    const hasChange = changed(connector, updates)
    if (!hasChange) {
        console.info(`No change to the connector. Done`)
        return
    }
    console.info(`There are changes to the connector. Update now.`)
    try {
        await v3.updateConnector(connectorId, updates)
    }
    catch (e) {
        console.error(`Exception when update connector: `, e)
        throw e
    }
    await upgradeIntegrations(connectorId)
}

const upgradeIntegrations = async (connectorId) => {
    const integrations = await v3.getIntegrations(connectorId)
    console.info(`Found ${integrations.length} integrations for the connector`)
    for (let i = 0; i < integrations.length; i++) {
        const integration = integrations[i]
        console.info(`Upgrading  ${i + 1} of ${integrations.length} integrations id: ${integration.integrationId}`)
        try {
            await v3.upgradeIntegration(integration.integrationId)
        }
        catch (e) {
            console.error(`Exception when upgrade integration: `, e)
        }
    }
}

const createWithKey = async (env, key, connectorDef) => {
    v3.setEnv(env)
    v3.setKey(key)
    let connector
    try {
        connector = await v3.createConnector(connectorDef)
    }
    catch(e) {
        console.error(`Exception when create connector: `, e)
        throw e
    }
    console.info(`Created connector: `, connector)
    const connectorId = connector.connectorId
    const keyDef = {
        name: `${connectorDef.connectorName} connector key`,
        connector: connectorId,
        tier: 'enterprise',
        environment: 'prod',
        methods: {
            integrations: 'rul',
            connectors: 'ru',
        },
        allowedIPs: [
            '0.0.0.0/0',
        ],
    }
    let createdKey
    try {
        createdKey = await v3.createConnectorKey(keyDef)
    }
    catch(e){
        console.error(`Exception when create key for connector: `, e)
        throw e
    }
    console.info(`Created connector key: `, createdKey)
}

module.exports = {
    update,
    createWithKey,
}
