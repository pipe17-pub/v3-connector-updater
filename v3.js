const axios = require('@pipe17-pub/axios-improved')

let v3url
const setEnv = (env) => {
    v3url = `https://api-v3.${env}.pipe17.com/api/v3`
    //TODO: remove once api-v3.master works
    if (env === 'master') {
        v3url = `https://api-v3.pipe17.com/api/v3`
    }
}

let options
const setKey = (key) => {
    options = {
        headers: {
            'x-pipe17-key': key,
        }
    }
}

const getIntegrations = async (connectorId) => {
    let skip = 0
    let count = 1000
    let integrations = []
    while(true) {
        const url = `${v3url}/integrations?count=1000&skip=${skip}&connectorId=${connectorId}`
        console.info(`Getting a batch of ${count} integrations: ${url}`)
        const res = await axios.get(url, options)
        if (!res.data || !res.data.integrations) {
            const msg = `No res.data or no integrations in res.data`
            console.error(msg)
            throw msg
        }
        const batchSize = res.data.integrations.length
        console.info(`Successfully got a batch of ${batchSize} integrations.`)
        integrations = integrations.concat(res.data.integrations)
        if (batchSize < count) {
            console.info(`We have got all integrations`)
            break
        }
        skip+=count
    }
    return integrations
}

const upgradeIntegration = async (integrationId) => {
    const url = `${v3url}/integrations/${integrationId}/upgrade`
    await axios.post(url, {}, options)
}

const getConnector = async (connectorId) => {
    const url = `${v3url}/connectors/${connectorId}`
    const res = await axios.get(url, options)
    if (res.data && res.data.connector) {
        return res.data.connector
    }
}

const updateConnector = async (connectorId, updates) => {
    const url = `${v3url}/connectors/${connectorId}`
    await axios.put(url, updates, options)
}

const createConnector = async (connectorDef) => {
    const url = `${v3url}/connectors`
    const res = await axios.post(url, connectorDef, options)
    if (res.data && res.data.connector) {
        return res.data.connector
    }
}

const createConnectorKey = async (keyDef) => {
    const url = `${v3url}/apikey`
    const res = await axios.post(url, keyDef, options)
    return res.data
}

module.exports = {
    setEnv,
    setKey,
    getIntegrations,
    upgradeIntegration,
    getConnector,
    updateConnector,
    createConnector,
    createConnectorKey,
}